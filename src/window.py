# window.py
#
# Copyright 2020 Nine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gst, GdkPixbuf
import urllib.parse
import ffmpeg


@Gtk.Template(resource_path='/com/github/nineh/avmacros-gui/window.ui')
class AvmacrosGuiWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'AvmacrosGuiWindow'

    current_file = None
    output_file = None
    file_width = None
    file_height = None
    xid = None
    pbin = None

    #main window widgets
    select_file = Gtk.Template.Child()
    render_button = Gtk.Template.Child()

    #trimming widgets
    enable_trim = Gtk.Template.Child()
    clip_play = Gtk.Template.Child()
    clip_start = Gtk.Template.Child()
    clip_length = Gtk.Template.Child()
    preview_area = Gtk.Template.Child()

    #color widgets
    enable_color = Gtk.Template.Child()
    color_mode_stack = Gtk.Template.Child()
    contrast_slider = Gtk.Template.Child()
    brightness_slider = Gtk.Template.Child()
    gamma_slider = Gtk.Template.Child()
    saturation_slider = Gtk.Template.Child()
    color_presets = Gtk.Template.Child()
    color_preview = Gtk.Template.Child()
    lut_chooser = Gtk.Template.Child()

    #stabilization widgets
    enable_stab = Gtk.Template.Child()
    shake_slider = Gtk.Template.Child()
    maxstep_slider = Gtk.Template.Child()
    smoothing_slider = Gtk.Template.Child()
    zoom_slider = Gtk.Template.Child()
    stab_algo = Gtk.Template.Child()
    stab_presets = Gtk.Template.Child()

    #framing widgets
    enable_crop = Gtk.Template.Child()
    aspect_ratio = Gtk.Template.Child()

    #slowmo/interp widgets
    enable_rates = Gtk.Template.Child()

    #output widgets

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.render_button.connect("clicked", self.save)
        self.select_file.connect("file-set", self.on_load)

        #set up trim ui

        self.preview_area.connect('realize', self.on_realize)
        self.clip_play.connect("clicked", self.preview_clip)
        self.clip_length.set_range(1,1000)
        self.clip_length.set_value(1)

        #set up color ui

        self.contrast_slider.set_range(0.0, 10.0)
        self.contrast_slider.set_value(1.0)
        self.brightness_slider.set_range(-1.0, 1.0)
        self.brightness_slider.set_value(0.0)
        self.gamma_slider.set_range(0.1, 10.0)
        self.gamma_slider.set_value(1.0)
        self.saturation_slider.set_range(0.0, 3.0)
        self.saturation_slider.set_value(1.0)

        self.contrast_slider.connect("value-changed", self.update_color_preview)
        self.brightness_slider.connect("value-changed", self.update_color_preview)
        self.gamma_slider.connect("value-changed", self.update_color_preview)
        self.saturation_slider.connect("value-changed", self.update_color_preview)

        #set up stabilization ui

        self.shake_slider.set_range(1, 10)
        self.shake_slider.set_value(5)
        self.maxstep_slider.set_range(1, 64)
        self.maxstep_slider.set_value(6)
        self.smoothing_slider.set_range(1, 64)
        self.smoothing_slider.set_value(10)
        self.zoom_slider.set_range(0.0, 1.0)
        self.zoom_slider.set_value(0.0)

        #set up crop ui


    def on_load(self, widget):
        self.current_file = urllib.parse.unquote(urllib.parse.urlparse(self.select_file.get_uri()).path)
        probe = ffmpeg.probe(self.current_file)
        video_stream = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
        self.file_width = int(video_stream['width'])
        self.file_height = int(video_stream['height'])

        self.preview_area.connect('draw', self.on_draw)
        #self.preview_clip(self)
        self.update_color_preview(self)

    def on_realize(self, widget, data=None):
        print("on_relaize")

        window = widget.get_window()
        self.xid = window.get_xid()


    def on_sync_message(self, bus, msg):
        if msg.get_structure().get_name() == 'prepare-window-handle':
            print('prepare-window-handle')

            print('on_sync', self.xid)
            self.pbin.set_window_handle(self.xid)

            print(msg)
            print(msg.src)

    def on_draw(self, widget, cr):
        cr.set_source_rgb(0, 0, 0)
        cr.rectangle(0, 0, self.file_width, self.file_height)
        cr.fill()


    def preview_clip(self, widget):
        Gst.init(None)
        pipeline = Gst.Pipeline()
        # audiosrc = Gst.ElementFactory.make("audiotestsrc", "buzzer")
        # audiosrc.set_property("freq",1000)
        # pipeline.add(audiotestsrc)
        # sink = Gst.ElementFactory.make("pulsesink", "pulseaudio_output")
        # pipeline.add(sink)
        # audiotestsrc.link(sink)
        self.pbin = Gst.ElementFactory.make('playbin', None)
        pipeline.add(self.pbin)
        bus = pipeline.get_bus()
        bus.enable_sync_message_emission()
        bus.connect('sync-message::element', self.on_sync_message)
        self.pbin.set_property("uri", self.select_file.get_uri())
        pipeline.set_state(Gst.State.PLAYING)


    def update_color_preview(self, widget):
        out, _ = (
            ffmpeg
            .input(self.current_file)
            .filter('eq',
                    contrast=self.contrast_slider.get_value(),
                    gamma=self.gamma_slider.get_value(),
                    brightness=self.brightness_slider.get_value(),
                    saturation=self.saturation_slider.get_value())
            .output('pipe:', vframes=1, format='rawvideo', pix_fmt='rgb24')
            .run(capture_stdout=True)
        )
        self.color_preview.set_from_pixbuf(GdkPixbuf.Pixbuf.new_from_data(out,
                                                GdkPixbuf.Colorspace.RGB,
                                                False,
                                                8,
                                                self.file_width,
                                                self.file_height,
                                                self.file_width*3).scale_simple(self.file_width/3, self.file_height/3, GdkPixbuf.InterpType.BILINEAR))


    def save(self, widget):
        dialog = Gtk.FileChooserDialog("Save your file", self,
                                       Gtk.FileChooserAction.SAVE,
                                       (Gtk.STOCK_CANCEL,Gtk.ResponseType.CANCEL,
                                        Gtk.STOCK_SAVE, Gtk.ResponseType.OK))

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.output_file = dialog.get_filename()
        dialog.destroy()

        self.render()


    def precalc_stabilization(self):
        (
            ffmpeg
            .input(self.current_file)
            .filter('vidstabdetect',
                    shakiness=self.shake_slider.get_value(),
                    stepsize=self.maxstep_slider.get_value())
            .output("null", f="null")
            .run()
        )


    def render(self):
        self.current_file = urllib.parse.unquote(urllib.parse.urlparse(self.select_file.get_uri()).path)

        if self.enable_stab.get_active():
            self.precalc_stabilization()

        stream = ffmpeg.input(self.current_file)

        if self.enable_trim.get_active():
            stream = ffmpeg.trim(stream, start_frame=0, end_frame=self.clip_length.get_value()*60)

        if self.enable_color.get_active():
            if (self.color_mode_stack.get_visible_child_name() == "color_adjust"):
                stream = ffmpeg.filter(stream, 'eq',
                                       contrast=self.contrast_slider.get_value(),
                                       gamma=self.gamma_slider.get_value(),
                                       brightness=self.brightness_slider.get_value(),
                                       saturation=self.saturation_slider.get_value())
            elif self.lut_chooser.get_uri() is not None:
                lut = urllib.parse.unquote(urllib.parse.urlparse(self.lut_chooser.get_uri()).path)
                stream = ffmpeg.filter(stream, 'lut3d', lut)

        if self.enable_stab.get_active():
            stream = ffmpeg.filter(stream, 'vidstabtransform',
                                   smoothing=self.smoothing_slider.get_value(),
                                   #zoom=self.zoom_slider.get_value(),
                                   optalgo=self.stab_algo.get_active_id(),
                                   input='transforms.trf')

        if self.enable_crop.get_active():
            pass
            #stream = ffmpeg.filter(stream, 'crop')
            #stream = ffmpeg.filter(stream, 'stretch')

        if self.enable_rates.get_active():
            pass

        stream = ffmpeg.output(stream, self.output_file)
        stream = ffmpeg.overwrite_output(stream)
        ffmpeg.run(stream)
