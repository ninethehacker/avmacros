# AVMacros

awesome app for getting clips from action cam and phone footage.

# Screenshots

`blah`

# Features

* clip trimming (incomplete)
* color correction/color grade
* stabilization
* reframing with superview/crop/stretch
* frame rate retargeting with lerp or slowmotion
* format and codec conversion (incomplete)
* dead simple codebase to hack on

# Installation

At this time I only support the project in GNOME builder until it is complete.

# TODO

* not setting app name or metadata for gnome
* for some reason chaining color and stabilization doesn't work
* get a cool icon
* finish preset code
* fix trim interface
* preview LUTs correctly
* implement superview
* figure out why ffmpeg is mangling bitrates
* set up project to support translation

**future**

* ship this motherlover
* figure out design to support time lapse photography again
* port to GNU\**slash*\*phone
* upgrade to GTK4 and see if that fixes gstreamer dependency
